<p align="center"><img src="https://img1.imgtp.com/2023/08/12/s9yRR4Vf.jpg" width="300" alt="小程序"></p>
<p align="center">扫码访问小程序</p>

## 关于图视界

"图视界"是一款基于微信小程序开发的图片应用程序，适合做图片展示和手机壁纸站等。

更多功能在进一步完善中，请随时关注更新情况。。。

## 联系作者

QQ：14898008

信箱：flyinglb@qq.com

## 后台展示

演示地址:
http://yourdomain/admin/login

<p align="center"><img src="https://img1.imgtp.com/2023/08/14/qxfb0jGK.png" alt="管理后台"></p>

<p align="center"><img src="https://img1.imgtp.com/2023/08/14/um6GINEC.png" alt="管理后台"></p>

<p align="center"><img src="https://img1.imgtp.com/2023/08/14/EY5tA52P.png" alt="管理后台"></p>

<p align="center"><img src="https://img1.imgtp.com/2023/08/14/hdbavZS0.png" alt="管理后台"></p>